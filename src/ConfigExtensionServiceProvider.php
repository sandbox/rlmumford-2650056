<?php

/**
 * @file
 * Contains Drupal\config_extension\ConfigExtensionServiceProvider
 */

namespace Drupal\config_extension;

use Drupal\Core\DependencyInjection\ServiceProviderBase;

class ConfigExtensionServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('config.factory');
    $definition->setClass('Drupal\config_extension\Config\ExtendableConfigFactory');
  }
}