<?php

/**
 * @file
 * Contains Drupal\config_extension\Config\ExtendableConfigFactory
 */

namespace Drupal\config_extension\Config;

use Drupal\Core\Config\Config;

class ExtendableConfig extends Config implements ExtendableConfigInterface {

  /**
   * The extendable status of the config object.
   *
   * @var string
   */
  protected $extendableStatus = 'custom';

  /**
   * The origin module of the config object.
   *
   * @var module;
   */
  protected $extendableModule;

  /**
   * Flag to store whether the default is being saved.
   *
   * @var bool
   */
  protected $storeDefault = FALSE;

  /**
   * {@inheritdoc}
   */
  public function save($has_trusted_data = FALSE) {
    if (!$this->storeDefault) {
      $this->extendableStatus = ($this->extendableStatus == 'default') ? 'overidden' : 'custom';
    }
    else if ($this->isNew && $this->exendableStatus == 'custom') {
      $this->extendableStatus = 'default';
    }

    if ($this->extendableStatus == 'default') {
      $this->eventDispatcher->dispatch('config_extension.alter', new ConfigCrudEvent($this));
    }

    // Set extendable metadata on the config data.
    $this->data['extendable_status'] = $this->extendableStatus;
    $this->data['extendable_module'] = $this->extendableModule;

    parent::save($has_trusted_data);
  }

  /**
   * {@inheritdoc}
   */
  public function initWithData(array $data) {
    $this->extendableStatus = $data['extendable_status'] ?: 'custom';
    unset($data['extendable_status']);
    $this->extendableModule = $data['extendable_module'] ?: '';
    unset($data['extendable_module']);
    parent::initWithData($data);
  }

  /**
   * Tell the config object that it is storing the default value.
   *
   * @param string $module
   *   The module that has provided this default configuration.
   */
  public function setStoreDefault($module = '') {
    $this->storeDefault = TRUE;
    $this->extendableModule = $module;
  }

  /**
   * Get the extendable status.
   *
   * @return string
   *   The extendable status of the config can be one of the followwing.
   *   - 'default': The configuration has been provided by alters and has not
   *                been edited by the site administrator. 'Default' config
   *                objects may change when new modules are installed.
   *   - 'custom': The configuration has been provided through action on the
   *               site.
   *   - 'overidden': The configuration has been provided by a module byt has
   *                  been edited by a site administrator. It will not be
   *                  changed.
   */
  public function getExtendableStatus() {
    return $this->extendableStatus;
  }

  /**
   * Get the extendable module.
   */
  public function getExtendableModule() {
    return $this->extendableModule;
  }

}