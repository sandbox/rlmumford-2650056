<?php

/**
 * @file
 * Contains Drupal\config_extension\Config\ExtendableConfigFactory
 */

namespace Drupal\config_extension\Config;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;

class ExtendableConfigFactory extends ConfigFactory {

  /**
   * {@inheritdoc}
   */
  protected function createConfigObject($name, $immutable) {
    if ($immutable) {
      return new ImmutableConfig($name, $this->storage, $this->eventDispatcher, $this->typedConfigManager);
    }
    return new ExtendableConfig($name, $this->storage, $this->eventDispatcher, $this->typedConfigManager);
  }
}