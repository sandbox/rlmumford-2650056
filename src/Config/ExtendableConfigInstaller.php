<?php

/**
 * @file
 * Contains Drupal\config_extension\Config\ExtendableConfigInstaller
 */

namespace Drupal\config_extension\Config;

use Drupal\Core\Config\ConfigInstaller;
use Drupal\Core\Config\StorageInterface;

class ExtendableConfigInstaller extends ConfigInstaller {

  /**
   * A stack of providers.
   *
   * This allows the config installer to know what the current provider is.
   *
   * @var array
   */
  protected $providerStack = [];

  /**
   * {@inheritdoc}
   */
  public function installDefaultConfig($type, $name) {
    array_push($this->providerStack, ['type' => $type, 'name' => $name]);
    parent::installDefaultConfig($type, $name);
    array_pop($this->providerStack);
  }

  /**
   * {@inheritdoc}
   */
  protected function createConfiguration($collection, array $config_to_create) {
    // Order the configuration to install in the order of dependencies.
    if ($collection == StorageInterface::DEFAULT_COLLECTION) {
      $dependency_manager = new ConfigDependencyManager();
      $config_names = $dependency_manager
        ->setData($config_to_create)
        ->sortAll();
    }
    else {
      $config_names = array_keys($config_to_create);
    }

    foreach ($config_names as $name) {
      // Allow config factory overriders to use a custom configuration object if
      // they are responsible for the collection.
      $overrider = $this->configManager->getConfigCollectionInfo()->getOverrideService($collection);
      if ($overrider) {
        $new_config = $overrider->createConfigObject($name, $collection);
      }
      else {
        $new_config = new ExtendableConfig($name, $this->getActiveStorages($collection), $this->eventDispatcher, $this->typedConfig);
      }
      if ($config_to_create[$name] !== FALSE) {
        $new_config->setData($config_to_create[$name]);
      }

      // Tell the config that what it is storing in the default value.
      if ($new_config instanceof ExtendableConfigInterface) {
        $provider = end($this->providerStack);
        $new_config->setStoreDefault($provider['name']);
      }

      if ($collection == StorageInterface::DEFAULT_COLLECTION && $entity_type = $this->configManager->getEntityTypeIdByName($name)) {
        // If we are syncing do not create configuration entities. Pluggable
        // configuration entities can have dependencies on modules that are
        // not yet enabled. This approach means that any code that expects
        // default configuration entities to exist will be unstable after the
        // module has been enabled and before the config entity has been
        // imported.
        if ($this->isSyncing()) {
          continue;
        }
        /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $entity_storage */
        $entity_storage = $this->configManager
          ->getEntityManager()
          ->getStorage($entity_type);
        // It is possible that secondary writes can occur during configuration
        // creation. Updates of such configuration are allowed.
        if ($this->getActiveStorages($collection)->exists($name)) {
          $id = $entity_storage->getIDFromConfigName($name, $entity_storage->getEntityType()->getConfigPrefix());
          $entity = $entity_storage->load($id);
          $entity = $entity_storage->updateFromStorageRecord($entity, $new_config->get());
        }
        else {
          $entity = $entity_storage->createFromStorageRecord($new_config->get());
        }
        if ($entity->isInstallable()) {
          $entity->trustData()->save();
        }
      }
      else {
        $new_config->save(TRUE);
      }
    }
  }

}