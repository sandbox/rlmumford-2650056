<?php

/**
 * @file
 * Contains Drupal\config_extension\Config\ExtendableConfigFactory
 */

namespace Drupal\config_extension\Config;

interface ExtendableConfigInterface {

  /**
   * Tell the config object that it is storing the default value.
   *
   * @param string $module
   *   The module that has provided this default configuration.
   */
  public function setStoreDefault($module = '');

  /**
   * Get the extendable status.
   *
   * @return string
   *   The extendable status of the config can be one of the followwing.
   *   - 'default': The configuration has been provided by alters and has not
   *                been edited by the site administrator. 'Default' config
   *                objects may change when new modules are installed.
   *   - 'custom': The configuration has been provided through action on the
   *               site.
   *   - 'overidden': The configuration has been provided by a module byt has
   *                  been edited by a site administrator. It will not be
   *                  changed.
   */
  public function getExtendableStatus();

  /**
   * Get the extendable module.
   */
  public function getExtendableModule();

}